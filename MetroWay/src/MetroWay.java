import javax.swing.*;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.*;

public class MetroWay extends JFrame {
	final int width = 800;
	final int height = 800;
	String SWsize; // store results from sanwich size radio buttons
	String Meat; // store results from Meat Type Combo Box
	String Bread; // store bread type from Bread Type Combo Box
	JTextArea summaryBox = new JTextArea(10, 10); // displays the transcript at the bottom
	JTextField enterName; // takes the user name as input
	String userName; // stores user name
	JComboBox ComboMeatType;
	JComboBox ComboBreadType;
	JLabel banner = new JLabel("MetroWay Sub Sandwiches");
	JLabel footer = new JLabel("Order Transcript");
	JPanel northPanel = new JPanel();
	JPanel southPanel = new JPanel();
	JPanel centerPanel = new JPanel();
	JPanel SWsizePanel = new JPanel();
	JPanel MeatTypePanel = new JPanel();
	JPanel BreadTypePanel = new JPanel();
	JPanel CondimentTypePanel = new JPanel();
	JPanel ControlPanel = new JPanel();
	JPanel userDataPanel = new JPanel();

	public MetroWay() {
		setTitle("MetroWay Sub Sandwiches");
		setSize(width, height);
		getContentPane().setLayout(new BorderLayout());

		buildNorthPanel();
		buildCenterPanel();
		buildSouthPanel();
		setVisible(true);

	}

	public void buildNorthPanel() {
		add(northPanel, BorderLayout.NORTH);
		northPanel.add(banner, BorderLayout.NORTH);
		Font font = new Font("Trebuchet MS", Font.BOLD, 26);
		banner.setFont(font);
		northPanel.setBackground(Color.BLUE);
		banner.setForeground(Color.WHITE);
	}

	public void buildCenterPanel() {
		add(centerPanel, BorderLayout.CENTER);
		centerPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		centerPanel.add(SWsizePanel);
		createSWsizePanel();
		centerPanel.add(MeatTypePanel);
		createMeatTypePanel();
		centerPanel.add(BreadTypePanel);
		createBreadTypePanel();
		centerPanel.add(CondimentTypePanel);
		createCondimentTypePanel();
		centerPanel.add(ControlPanel);
		createTimeSlider();
		centerPanel.add(userDataPanel);
		userData();
		JPanel orderPanel = new JPanel();
		orderPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		orderPanel.setBackground(Color.WHITE);
		orderPanel.setPreferredSize(new Dimension(780, 75));
		Font font = new Font("Tahoma", Font.BOLD, 12);
		JLabel orderLabel = new JLabel("-- Order Summary --");
		orderLabel.setFont(font);
		centerPanel.add(orderPanel);
		orderPanel.add(orderLabel);

	}

	public void createSWsizePanel() {
		SWsizePanel.setPreferredSize(new Dimension(780, 75));

		SWsizePanel.setBackground(Color.WHITE);
		SWsizePanel.setBorder(new TitledBorder(new EtchedBorder(),
				"Select a Sandwich size"));
		JLabel SWsize = new JLabel("Sandwich Size:  ");
		SWsizePanel.add(SWsize);
		JRadioButton rSize1 = new JRadioButton("6 inch");
		rSize1.setBackground(Color.WHITE);
		rSize1.setActionCommand("6inch");
		rSize1.addActionListener(new RadioButtonListener());
		SWsizePanel.add(rSize1);
		JRadioButton rSize2 = new JRadioButton("12 inch");
		rSize2.setBackground(Color.WHITE);
		rSize2.setActionCommand("12inch");
		rSize2.addActionListener(new RadioButtonListener());
		SWsizePanel.add(rSize2);
		ButtonGroup radioButtons = new ButtonGroup();
		radioButtons.add(rSize1);
		radioButtons.add(rSize2);
	}

	private class RadioButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			String choice = e.getActionCommand();
			if (choice.equals("6inch"))
				SWsize = "6inch";
			if (choice.equals("12inch"))
				SWsize = "12inch";

			// TODO Auto-generated method stub

		}

	}

	private class ButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			String choice = e.getActionCommand();
			if (choice.equals("submit"))
				userName=enterName.getText();
				summaryBox.append("Customer Name :" +userName+"\n");
				summaryBox.append("SandWich Size : " + SWsize + "\n");
			summaryBox.append("Meat Type :" + Meat + "\n");
			summaryBox.append("Bread Type :" + Bread + "\n");

			if (choice.equals("reset"))
				JOptionPane.showMessageDialog(null, "it was reset");

			// TODO Auto-generated method stub

		}

	}

	private class ComboBoxListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			Object ComboMeatType = (JComboBox) e.getSource();
			Object choice = ((JComboBox) ComboMeatType).getSelectedItem();
		}
	}

	public void createMeatTypePanel() {
		MeatTypePanel.setPreferredSize(new Dimension(780, 75));
		MeatTypePanel.setBackground(Color.WHITE);
		MeatTypePanel.setBorder(new TitledBorder(new EtchedBorder(),
				"Select a Meat Type"));
		JLabel MeatType = new JLabel("Meat Type:  ");
		MeatTypePanel.add(MeatType);
		ComboMeatType = new JComboBox();
		ComboMeatType.addItem("Ham");
		ComboMeatType.addItem("Turkey");
		ComboMeatType.addItem("Roast Beef");
		ComboMeatType.addItem("Chicken");
		ComboMeatType.addItem("Tuna");
		ComboMeatType.addItemListener(new ItemListener() {

			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED)
					Meat = (String) ComboMeatType.getSelectedItem();

			}
		});
		MeatTypePanel.add(ComboMeatType);
	}

	public void createBreadTypePanel() {
		BreadTypePanel.setPreferredSize(new Dimension(780, 75));
		BreadTypePanel.setBackground(Color.WHITE);
		BreadTypePanel.setBorder(new TitledBorder(new EtchedBorder(),
				"Select a Bread Type"));
		JLabel BreadType = new JLabel("Bread Type:  ");
		BreadTypePanel.add(BreadType);
		ComboBreadType = new JComboBox();
		ComboBreadType.addItem("Italian");
		ComboBreadType.addItem("Wheat");
		ComboBreadType.addItem("Cheddar Baked");
		ComboBreadType.addItem("Toasted Oat");
		ComboBreadType.addItem("Rye");
		ComboBreadType.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED)
					Bread = (String) ComboBreadType.getSelectedItem();
			}
		});
		BreadTypePanel.add(ComboBreadType);
	}

	public void createCondimentTypePanel() {
		CondimentTypePanel.setPreferredSize(new Dimension(780, 75));
		CondimentTypePanel.setBackground(Color.WHITE);
		CondimentTypePanel.setBorder(new TitledBorder(new EtchedBorder(),
				"Select Condiment Types"));
		JCheckBox CBcond1 = new JCheckBox("Ranch");
		CondimentTypePanel.add(CBcond1);
		CBcond1.setBackground(Color.WHITE);
		JCheckBox CBcond2 = new JCheckBox("Mayo");
		CondimentTypePanel.add(CBcond2);
		CBcond2.setBackground(Color.WHITE);
		JCheckBox CBcond3 = new JCheckBox("Mustard");
		CondimentTypePanel.add(CBcond3);
		CBcond3.setBackground(Color.WHITE);
		JCheckBox CBcond4 = new JCheckBox("Ketchup");
		CondimentTypePanel.add(CBcond4);
		CBcond4.setBackground(Color.WHITE);
		JCheckBox CBcond5 = new JCheckBox("BBQ Sauce");
		CondimentTypePanel.add(CBcond5);
		CBcond5.setBackground(Color.WHITE);

	}

	public void buildSouthPanel() {
		add(southPanel, BorderLayout.SOUTH);

		summaryBox.setBorder(new TitledBorder(new EtchedBorder()));
		southPanel.add(summaryBox);
		summaryBox.setEditable(false);

	}

	public void createTimeSlider() {
		ControlPanel.setBorder(new TitledBorder(new EtchedBorder(),
				"Set Pick Up Time"));
		ControlPanel.setPreferredSize(new Dimension(780, 110));
		ControlPanel.setBackground(Color.WHITE);
		JSlider timeSlider = new JSlider(0, 60, 15);
		timeSlider.setPreferredSize(new Dimension(500, 70));
		timeSlider.setBackground(Color.WHITE);
		JLabel timeSliderLabel = new JLabel("Pick Up time:  ");
		ControlPanel.add(timeSliderLabel);
		ControlPanel.add(timeSlider);
		JLabel timeSliderLabe2 = new JLabel("  Minutes");
		ControlPanel.add(timeSliderLabe2);
		timeSlider.setMajorTickSpacing(5);
		timeSlider.setMinorTickSpacing(1);
		timeSlider.setPaintTicks(true);
		timeSlider.setPaintLabels(true);
	}

	public void userData() {

		String spacer = "                                     ";
		userDataPanel
				.setBorder(new TitledBorder(new EtchedBorder(), "Finalize"));
		userDataPanel.setPreferredSize(new Dimension(780, 75));
		userDataPanel.setBackground(Color.WHITE);
		JLabel name = new JLabel("Enter Name");
		userDataPanel.add(name);
		enterName = new JTextField(20);
		userDataPanel.add(enterName);
		userDataPanel.add(new JLabel(spacer));
		JButton reset = new JButton("Reset");
		reset.setActionCommand("reset");
		reset.addActionListener(new ButtonListener());
		userDataPanel.add(reset);

		JButton submit = new JButton("Submit");
		submit.setActionCommand("submit");
		submit.addActionListener(new ButtonListener());
		userDataPanel.add(submit);

	}

	public static void main(String[] args) {
		MetroWay order = new MetroWay();
	}
}
