/**
 *Represents a MetroWay Sub Cash Register Program
 *Written by Saksham Ghimire for Programming Assignment 3
 *November 21, 2010
 */
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSlider;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * This Class implements MetroWay Sub Cash Register Program User is able to
 * selcted the type of Sandwich with choice of Bread,meat and condiments The
 * programs calculates the total amount and generates a final transcript
 * accordingly
 */
public class MetroWay extends JFrame {
	final int width = 800; // frame width
	final int height = 850; // frame height
	int sliderValue; // store JSliders value
	private String SW; // stores sandwich size
	private String Meat; // store results from Meat Type Combo Box
	private String Bread; // store bread type from Bread Type Combo Box
	JTextArea summaryBox; // displays the transcript at the bottom
	JPanel orderPanel;
	JTextField enterName; // takes the user name as input
	String userName; // stores user name
	JComboBox ComboMeatType; // Combo box for selecting meat type
	JComboBox ComboBreadType; // combo box for selecting bread type
	JSlider slider; // JSlider for getting minute value
	ButtonGroup radioButtons; // group of radio buttons.
	JRadioButton rSize1; // 6 inch radio button
	JRadioButton rSize2; // 12 inch radio Button
	JButton submit; // submit button
	JButton reset; // submit button
	JCheckBox condiment1; // condiment type 1
	JCheckBox condiment2; // condiment type 2
	JCheckBox condiment3; // condiment type 3
	JCheckBox condiment4; // condiment type 4
	JCheckBox condiment5; // condiment type 5
	JTextArea totalBox; // displays the total amount due
	JLabel totalLabel; // displays the total label
	JLabel banner = new JLabel("MetroWay Sub Sandwiches");
	JLabel footer = new JLabel("Order Transcript");
	JPanel northPanel = new JPanel();
	JPanel southPanel = new JPanel();
	JPanel centerPanel = new JPanel();
	JPanel SWsizePanel = new JPanel();
	JPanel MeatTypePanel = new JPanel();
	JPanel BreadTypePanel = new JPanel();
	JPanel CondimentTypePanel = new JPanel();
	JPanel ControlPanel = new JPanel();
	JPanel userDataPanel = new JPanel();

	// Constructor for initializing the Container
	public MetroWay() {
		setTitle("MetroWay Sub Sandwiches");
		setSize(width, height);
		getContentPane().setLayout(new BorderLayout());

		buildNorthPanel();
		buildCenterPanel();
		buildSouthPanel();
		setVisible(true);

	}

	// Creates north Panel that holds Banner and Headers
	public void buildNorthPanel() {
		add(northPanel, BorderLayout.NORTH);
		northPanel.add(banner, BorderLayout.NORTH);
		Font font = new Font("Trebuchet MS", Font.BOLD, 26);
		banner.setFont(font);
		northPanel.setBackground(Color.BLUE);
		banner.setForeground(Color.WHITE);
	}

	// Creates a Center Panel that holds RadioButtons, ComboBox and CheckBox
	public void buildCenterPanel() {
		add(centerPanel, BorderLayout.CENTER);
		centerPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		centerPanel.add(SWsizePanel);
		createSWsizePanel();
		centerPanel.add(MeatTypePanel);
		createMeatTypePanel();
		centerPanel.add(BreadTypePanel);
		createBreadTypePanel();
		centerPanel.add(CondimentTypePanel);
		createCondimentTypePanel();
		centerPanel.add(ControlPanel);
		createTimeSlider();
		centerPanel.add(userDataPanel);
		userData();
		orderPanel = new JPanel();
		orderPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		orderPanel.setBackground(Color.WHITE);
		orderPanel.setPreferredSize(new Dimension(780, 75));
		Font font = new Font("Tahoma", Font.BOLD, 12);
		JLabel orderLabel = new JLabel("-- Order Summary --");
		orderLabel.setFont(font);
		centerPanel.add(orderPanel);
		orderPanel.add(orderLabel);
		orderPanel.setVisible(false);

	}

	// Creates a Sandwich panel to select Sandwich type
	public void createSWsizePanel() {
		SWsizePanel.setPreferredSize(new Dimension(780, 75));

		SWsizePanel.setBackground(Color.WHITE);
		SWsizePanel.setBorder(new TitledBorder(new EtchedBorder(),
				"Select a Sandwich size"));
		JLabel SWsize = new JLabel("Sandwich Size:  ");
		SWsizePanel.add(SWsize);
		rSize1 = new JRadioButton("6 inch", true);
		rSize1.setBackground(Color.WHITE);

		SW = "6 inch";
		// listener for first radio button
		rSize1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				if (rSize1.isSelected())
					SW = "6 inch";

			}
		});
		SWsizePanel.add(rSize1);
		rSize2 = new JRadioButton("12 inch");
		rSize2.setBackground(Color.WHITE);
		// listener for the second radio button
		rSize2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				if (rSize2.isSelected())
					SW = "12 inch";

			}
		});
		SWsizePanel.add(rSize2);
		// Creating a Button Group
		ButtonGroup radioButtons = new ButtonGroup();
		radioButtons.add(rSize1);
		radioButtons.add(rSize2);

	}

	// Change Listener for JSlider that records time
	private class SliderListener implements ChangeListener {
		public void stateChanged(ChangeEvent evt) {
			JSlider slider = (JSlider) evt.getSource();

			if (!slider.getValueIsAdjusting()) {
				// Get new value
				sliderValue = slider.getValue();

			}
		}
	}

	// Button Listener for SUBMIT Button
	private class ButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			String choice = e.getActionCommand();
			String condimentOutput = "";
			double meatprice = 0;
			double cprice = 0;
			southPanel.setVisible(true);
			orderPanel.setVisible(true);
			if (choice.equals("submit")) {
				userName = enterName.getText();
			}

			// conditions for fetching checked state of JCheckBox
			if (condiment1.isSelected()) {
				condimentOutput += "Mayo, ";
				cprice += 0.25;
			}
			if (condiment2.isSelected()) {
				condimentOutput += "Onions, ";
				cprice += 0.25;
			}
			if (condiment3.isSelected()) {
				condimentOutput += "Peppers, ";
				cprice += 0.25;
			}
			if (condiment4.isSelected()) {
				condimentOutput += "Olives, ";
				cprice += 0.25;
			}
			if (condiment5.isSelected()) {
				condimentOutput += "Mushrooms ";
				cprice += 0.25;
			}
			// returning selected item from the Meat Combo Box
			Meat = (String) ComboMeatType.getSelectedItem();
			// returning selected item from the Bread Combo Box
			Bread = (String) ComboBreadType.getSelectedItem();
			// Calculate the Bread and Meat Prices
			if (SW == "6 inch" && Meat == "Ham")
				meatprice = 3.99;
			else if (SW == "12 inch" && Meat == "Ham")
				meatprice = 5.99;
			if (SW == "6 inch" && Meat == "Turkey")
				meatprice = 4.29;
			else if (SW == "12 inch" && Meat == "Turkey")
				meatprice = 5.39;
			if (SW == "6 inch" && Meat == "Roast Beef")
				meatprice = 4.99;
			else if (SW == "12 inch" && Meat == "Roast Beef")
				meatprice = 6.89;
			if (SW == "6 inch" && Meat == "Chicken")
				meatprice = 4.79;
			else if (SW == "12 inch" && Meat == "Chicken")
				meatprice = 6.79;
			if (SW == "6 inch" && Meat == "Tuna")
				meatprice = 3.99;
			else if (SW == "12 inch" && Meat == "Tuna")
				meatprice = 5.49;
			double total = meatprice + cprice;
			Font thisFont = new Font("Serif", Font.BOLD, 14);
			summaryBox.setFont(thisFont);
			// Append all output values for generating final Transcript
			summaryBox.append("Customer Name :" + userName + "\n");
			summaryBox.append("SandWich Size : " + SW + "\n");
			summaryBox.append("Meat Type : " + Meat + "  @ $" + meatprice
					+ "\n");
			summaryBox.append("Bread Type : " + Bread + "\n");
			if (condimentOutput.equals("")) {
				summaryBox.append("Condiments : none\n");
			} else {
				summaryBox.append("Condiments : " + condimentOutput
						+ "  @ $0.25 each." + "\n");
			}
			summaryBox.append("Pick-Up Time : " + sliderValue + " minutes\n");
			summaryBox
					.append("=======================================================\n");
			summaryBox.append("Subtotal : $ " + total + "\n");
			// Calculate Total and add respective tax amount
			summaryBox
					.append((String.format("Tax : $ %1.2f ", (total * 0.055)))
							+ " @ 5.5% \n");
			totalBox.setText(String.format(" $ %1.2f", (total * (1 + 0.055))));
			submit.setEnabled(false); // disables the submit button
		}
	}

	// Action Listener for RESET button
	private class ButtonListener1 implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			String choice1 = e.getActionCommand();
			if (choice1.equals("reset")) {

				summaryBox.setText("");
				totalBox.setText("");
				rSize1.setSelected(true);
				SW = "6 inch";
				ComboMeatType.setSelectedItem("Ham");
				Meat = "";
				ComboBreadType.setSelectedItem("Italian");
				Bread = "";
				condiment1.setSelected(false);
				condiment2.setSelected(false);
				condiment3.setSelected(false);
				condiment4.setSelected(false);
				condiment5.setSelected(false);
				slider.setValue(15);
				sliderValue = 15;
				userName = "";
				enterName.setText("");
				southPanel.setVisible(false); // hides the southPanel
				orderPanel.setVisible(false);// hides the orderPanel
				submit.setEnabled(true);// activates the submit button for use
			}
		}
	}

	// Creates a Panel for selecting Meat Type

	public void createMeatTypePanel() {
		MeatTypePanel.setPreferredSize(new Dimension(780, 75));
		MeatTypePanel.setBackground(Color.WHITE);
		MeatTypePanel.setBorder(new TitledBorder(new EtchedBorder(),
				"Select a Meat Type"));
		JLabel MeatType = new JLabel("Meat Type:  ");
		MeatTypePanel.add(MeatType);
		ComboMeatType = new JComboBox();
		ComboMeatType.addItem("Ham");
		ComboMeatType.addItem("Turkey");
		ComboMeatType.addItem("Roast Beef");
		ComboMeatType.addItem("Chicken");
		ComboMeatType.addItem("Tuna");
		MeatTypePanel.add(ComboMeatType);
	}

	// Creates a Panel for selecting Bread Type
	public void createBreadTypePanel() {
		BreadTypePanel.setPreferredSize(new Dimension(780, 75));
		BreadTypePanel.setBackground(Color.WHITE);
		BreadTypePanel.setBorder(new TitledBorder(new EtchedBorder(),
				"Select a Bread Type"));
		JLabel BreadType = new JLabel("Bread Type:  ");
		BreadTypePanel.add(BreadType);
		ComboBreadType = new JComboBox();
		ComboBreadType.addItem("Italian");
		ComboBreadType.addItem("Wheat");
		ComboBreadType.addItem("Cheddar Baked");
		ComboBreadType.addItem("Toasted Oat");
		ComboBreadType.addItem("Rye");
		BreadTypePanel.add(ComboBreadType);
	}

	// Creates a Panel for selecting Condiment Type
	public void createCondimentTypePanel() {
		CondimentTypePanel.setPreferredSize(new Dimension(780, 75));
		CondimentTypePanel.setBackground(Color.WHITE);
		CondimentTypePanel.setBorder(new TitledBorder(new EtchedBorder(),
				"Select Condiment Types"));
		condiment1 = new JCheckBox("Mayo");
		CondimentTypePanel.add(condiment1);
		condiment1.setBackground(Color.WHITE);
		condiment2 = new JCheckBox("Onions");
		CondimentTypePanel.add(condiment2);
		condiment2.setBackground(Color.WHITE);
		condiment3 = new JCheckBox("Peppers");
		CondimentTypePanel.add(condiment3);
		condiment3.setBackground(Color.WHITE);
		condiment4 = new JCheckBox("Olives");
		CondimentTypePanel.add(condiment4);
		condiment4.setBackground(Color.WHITE);
		condiment5 = new JCheckBox("Mushrooms");
		CondimentTypePanel.add(condiment5);
		condiment5.setBackground(Color.WHITE);

	}

	// Creates a South Panel that includes total amount and SummaryBox
	public void buildSouthPanel() {
		add(southPanel, BorderLayout.SOUTH);
		summaryBox = new JTextArea(10, 40);
		summaryBox.setBorder(new TitledBorder(new EtchedBorder()));
		southPanel.add(summaryBox);
		summaryBox.setEditable(false);
		totalLabel = new JLabel("Total ");
		totalLabel.setFont(new Font("Georgia", Font.BOLD, 30));
		totalLabel.setForeground(Color.BLUE);
		totalBox = new JTextArea(1, 5);
		totalBox.setFont(new Font("Georgia", Font.BOLD, 30));
		totalBox.setForeground(Color.RED);
		totalBox.setEditable(false);
		totalBox.setBorder(new TitledBorder(new EtchedBorder()));
		southPanel.add(totalLabel);
		southPanel.add(totalBox);
		southPanel.setVisible(false);

	}

	// Creates a Time Slider for recording time in minutes
	
	public void createTimeSlider() {
		ControlPanel.setBorder(new TitledBorder(new EtchedBorder(),
				"Set Pick Up Time"));
		ControlPanel.setPreferredSize(new Dimension(780, 110));
		ControlPanel.setBackground(Color.WHITE);
		slider = new JSlider(0, 60, 15);
		sliderValue = 15;
		slider.addChangeListener(new SliderListener());
		slider.setPreferredSize(new Dimension(500, 70));
		slider.setBackground(Color.WHITE);
		JLabel timeSliderLabel = new JLabel("Pick Up time:  ");

		ControlPanel.add(timeSliderLabel);
		ControlPanel.add(slider);
		JLabel timeSliderLabe2 = new JLabel("  Minutes");
		ControlPanel.add(timeSliderLabe2);
		slider.setMajorTickSpacing(5);
		slider.setMinorTickSpacing(1);
		slider.setPaintTicks(true);
		slider.setPaintLabels(true);
	}

	// User Data panel for Handling user name, Submit and Reset Buttons
	public void userData() {

		String spacer = "                                     ";
		userDataPanel
				.setBorder(new TitledBorder(new EtchedBorder(), "Finalize"));
		userDataPanel.setPreferredSize(new Dimension(780, 75));
		userDataPanel.setBackground(Color.WHITE);
		JLabel name = new JLabel("Enter Name");
		userDataPanel.add(name);
		// create a new TextField with maximum 20 characters
		enterName = new JTextField(20);

		userDataPanel.add(enterName);
		userDataPanel.add(new JLabel(spacer));
		reset = new JButton("Reset");
		reset.setActionCommand("reset");
		reset.addActionListener(new ButtonListener1());
		userDataPanel.add(reset);

		submit = new JButton("Submit");
		submit.setActionCommand("submit");
		submit.addActionListener(new ButtonListener());
		userDataPanel.add(submit);

	}

	// Main Method
	public static void main(String[] args) {
		JFrame.setDefaultLookAndFeelDecorated(true); // Frame decorations
		MetroWay order = new MetroWay();
	}// end Main
} // end Class
